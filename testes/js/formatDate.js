// Escreva uma função que converta a data de entrada do usuário formatada como MM/DD/YYYY em um formato exigido por uma API (YYYYMMDD). O parâmetro "userDate" e o valor de retorno são strings.

// Por exemplo, ele deve converter a data de entrada do usuário "31/12/2014" em "20141231" adequada para a API.

//No enunciado diz MM/DD/YYYY, o exemplo acima que diz '31/12/2014' está em DD/MM/YYYY, mas não interferiu em nada!!!!

function formatDate(userDate) {
    var date = new Date(userDate);
    month = parseInt(date.getMonth()) < 9 ? '0' + (date.getMonth()+1)  : '' + (date.getMonth() + 1);
    day = parseInt(date.getDate()) < 10 ? '0' + day : '' + date.getDate();
    year = date.getFullYear();

    return year+month+day;
}

console.log(formatDate("12/31/2014"));