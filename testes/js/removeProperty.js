// Implemente a função removeProperty, que recebe um objeto e o nome de uma propriedade.

// Faça o seguinte:

// Se o objeto obj tiver uma propriedade prop, a função removerá a propriedade do objeto e retornará true;
// em todos os outros casos, retorna falso.

function removeProperty(obj, prop) {
	if(obj.hasOwnProperty(prop)){  	//Verifica se existe uma propriedade 'prop' no objeto 'obj'
		delete obj.prop;			//Caso tenha, a remove do objeto	
		return true;				//E retorna true
	}
  	return false;					//Se não tiver a propriedade, retorna false
}