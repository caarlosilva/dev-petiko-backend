<?php


/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*

Postmon é uma API para consultar CEP e encomendas de maneira fácil.

Implemente uma função que recebe CEP e retorna todos os dados reltivos ao endereço correspondente.

Exemplo:

getAddressByCep('13566400') retorna:
{
	"bairro": "Vila Marina",
	"cidade": "São Carlos",
	"logradouro": "Rua Francisco Maricondi",
	"estado_info": {
	"area_km2": "248.221,996",
	"codigo_ibge": "35",
		"nome": "São Paulo"
	},
	"cep": "13566400",
	"cidade_info": {
		"area_km2": "1136,907",
		"codigo_ibge": "3548906"
	},
	"estado": "SP"
}



Documentação:
https://postmon.com.br/


*/

class CEP
{
    public static function getAddressByCep($cep)
    {
    	//Aparentemente tem diversas formas de se fazer uma requisição REST, a forma que achei foi através do cURL
		$ch = curl_init(); //Inicia a sessão do CURL
		//Configurando as opções da requisição, tais como o link para fazer a requisição a API. 
		curl_setopt($ch, CURLOPT_URL, 'https://api.postmon.com.br/v1/cep/'.$cep);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		// Faz a requisição ao navegador
		curl_exec($ch);
		//Encerrar o cURL e libera recursos do sistema
		curl_close($ch);
    }
}


var_dump(CEP::getAddressByCep('13566400'));