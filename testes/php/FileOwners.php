<?php


/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Implemente uma função que ao receber um array associativo contendo arquivos e donos, retorne os arquivos agrupados por dono. 

Por exemplo, um array ["Input.txt" => "Jose", "Code.py" => "Joao", "Output.txt" => "Jose"] a função groupByOwners deveria retornar ["Jose" => ["Input.txt", "Output.txt"], "Joao" => ["Code.py"]].


*/

class FileOwners
{
    public static function groupByOwners($files)
    {
    	$owners = []; //array que será agrupado por 'donos' agora
       	foreach ($files as $key => $value) {	//itera-se os arquivos com suas chave=>valor
       		$owners[$value][]=$key;	//para cada Dono, cria-se um novo array, que receberá os arquivos
       	}	
       	return $owners;
    }
}

$files = array
(
    "Input.txt" => "Jose",
    "Code.py" => "Joao",
    "Output.txt" => "Jose",
    "Click.js" => "Maria",
    "Out.php" => "maria", //No caso, "Maria" está sendo considerada um owner diferente de "maria"
    

);
var_dump(FileOwners::groupByOwners($files));
?>